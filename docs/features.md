# Features

!!! attention "Observação"
    Api será referenciada como sistema externo como {$api}.

## **Autenticação**


Deve disponibilizar uma requisição de login com username e password, onde é enviado credenciais para a api (_**authentication**_), essa api faz a validação das credenciais recebidas, e se forem válidas, gera um token para o usuário, o token percorre o caminho de volta através do _**api gateway**_ para a _**aplicação**_, onde o token é salvo para ser usado em novas requisições e o acesso do usuário é permitido.

### Exemplo URL:

{$api}/login

### Envio:

| Método | Header                                                                                       |
| ------ | -------------------------------------------------------------------------------------------- |
| POST   | [Authorization: Basic **base64(username:password)**](https://www.rfc-editor.org/rfc/rfc7617) |

### Retorno:

=== "**Status 200 (Sucesso)**"

    ```JSON {linenums="1"}
    {
        "access_token": "token válido",
        "token_type":"Bearer"
    }
    ```

=== "**Status 401 (Falha)**"

    ```json {linenums="1"}
    {
        "message_error": "unauthorized"
    }
    ```

## **Autorização**

Para autorizar o acesso do usuário a conteúdos na _**aplicação**_ que exigem um determinado nível de acesso, utilizamos o token que foi gerado e salvo no login, este token é enviado pela aplicação a cada requisição para a _**api**_ via _**bearer authorization**_ para autorização.

!!! attention "Observação"

    Para realizar a autorização o campo token é obrigatório!

### Envio:

| Método | Body                                                                                    |
| ------ | --------------------------------------------------------------------------------------- |
| POST   | Content-type *application/x-www-form-urlencoded*<br />token  **(token a ser validado)** |

### Retorno: 

=== "**Status 200 (Sucesso)**"

    ```json {linenums="1"}
    {
        "name":"informacoes a serem definidas",
        "iat": 1675275864,
        "exp": 1675276464,
        "active": true
    }
    ```

=== "**Status 200 (Falha)**"

    ```json {linenums="1"}
    {
      "active": false
    }
    ```


![Inspira Kab Flux](assets/images/diagrams/general_flux.jpg)

## **Busca Por Cliente**

![Inspira Kab Flux](assets/images/diagrams/search_flux.jpg)

Deve ser disponibilizado uma requisição para consumir dados e realizar a busca por cliente, a _**aplicação**_ envia requisições para a _**api**_, onde é feita introspecção do token (ver **Login**), caso validado o token, o acesso dos dados é feito e são retornados para a aplicação;

!!! attention "Observação"

    Para realizar a busca por cliente o campo token é obrigatório!

### Exemplo URL:

{$api}/cliente/cpf/73178314687 ou

{$api}/cliente/id/1001

### Envio:

| Método | Auth                               |
| ------ | ---------------------------------- |
| GET    | Bearer Token *token*               |

### Retorno:

=== "**Status 200 (Sucesso)**"

    ```json {linenums="1"}
    {
      "id": 1001,
      "nome": "Ricky Wisozk",
      "email": "orangegiraffe54@gmail.com",
      "endereco": "93448 Walsh Lodge",
      "nascimento": "1970-07-20",
      "cpf": "12175615952",
      "celular": "(68)92304-4078",
      "limite":1000000,
      "limiteDisponivel": 1000000,
      "status": "ativo",
      "statusMessage": "O cartão está ATIVO",
      "cartao": {
        "vencimento": "06/2029",
        "diaCorte": "08"
      }
    }
    ```

=== "**Status 400 (Falha)**"

    ```json {linenums="1"}
    {
      "message": "Id não encontrado."
    }
    ```

=== "**Status 401 (Falha)**"

    ```json {linenums="1"}
    {
      "statusCode": 401,
      "error": "Unauthorized",
      "message": "Token nao informado!"
    }
    ```

## **Saque Assistido**


![Inspira Kab Flux](assets/images/diagrams/withdraw_bank_flux.jpg)

![Inspira Kab Flux](assets/images/diagrams/withdraw_store_flux.jpg)

Deve ser disponibilizado uma requisição para consumir dados e realizar a transação de saque, a _**aplicação**_ envia requisições para a _**api**_, onde é feita introspecção do token (ver **Login**), caso validado o token, o consumo e envio dos dados é feito;

### Saque Assistido 0.1 - Listagem Dos Bancos:

!!! attention "Observação"

    Para realizar a transação de saque o campo token é obrigatório!

### Exemplo URL:

{$api}/banco

### Envio:

| Método | Auth                               |
| ------ | ---------------------------------- |
| GET    | Bearer Token *token*               |

### Retorno:

=== "**Status 200 (Sucesso)**"

    ```json {linenums="1"}
    [
        {
            "numero": "001",
            "nome": "Banco do Brasil S.A."
        },
        {
            "numero": "003",
            "nome": "Banco da Amazônia S.A."
        },
        {
            "numero": "004",
            "nome": "Banco do Nordeste do Brasil S.A."
        },
        ...
    ]
    ```

=== "**Status 401 (Falha)**"

    ```json {linenums="1"}
    {
      "statusCode": 401,
      "error": "Unauthorized",
      "message": "Token nao informado!"
    }
    ```

### Saque Assistido 0.2 - Listagem Tipo Planos:

!!! attention "Observação"

    Para realizar a transação de saque o campo token é obrigatório!

### Exemplo URL:

{$api}/prestacoes/cliente/1003/valor/10000/tipo/saque-loja ou transferencia-bancaria

### Envio:

| Método | Auth                               |
| ------ | ---------------------------------- |
| GET    | Bearer Token *token*               |

### Retorno:

=== "**Status 200 (Sucesso)**"

    ```json {linenums="1"} 
    [
        {
            "quantidade": 3,
            "valor": 3333,
            "juros": 150,
            "dataPrimeira": "2023-02-14"
        },
        {
            "quantidade": 6,
            "valor": 1666,
            "juros": 300,
            "dataPrimeira": "2023-02-14"
        },
        {
            "quantidade": 9,
            "valor": 1111,
            "juros": 450,
            "dataPrimeira": "2023-02-14"
        },
        {
            "quantidade": 12,
            "valor": 833,
            "juros": 600,
            "dataPrimeira": "2023-02-14"
        }
    ]
    ```

=== "**Status 401 (Falha)**"

    ```json {linenums="1"}
    {
      "statusCode": 401,
      "error": "Unauthorized",
      "message": "Token nao informado!"
    }
    ```

### Saque Assistido 0.3 - Realizar Saque:

!!! attention "Observação"

    Para realizar a transação de saque o campo token é obrigatório!

### Exemplo URL:

{$api}/cliente/saque/1003

### Envio:

| Método | Auth                               |
| ------ | ---------------------------------- |
| PUT    | Bearer Token *token*               |

=== "**Body (JSON de envio Transferência bancária)**"

    ```json {linenums="1"}
    {
       "idCliente": "1010",
       "parcela": {
          "quantidade": 6,
          "valor": 1833,
          "juros": 300,
          "dataPrimeira": "2023-03-02"
       },
       "banco": {
          "value": "004",
          "label": "Banco do Nordeste do Brasil S.A.",
          "conta": {
             "tipo": "Corrente",
             "agencia": "22222",
             "numeroConta": "12345"
          }
       },
       "valor": 11000,
       "tipo": "transferencia-bancaria",
       "data": "2023-03-02"
    }
    ```

=== "**Body (JSON de envio Saque em loja)**"

    ```json {linenums="1"}
    {
       "idCliente": "1010",
        "parcela": {
            "quantidade": 6,
            "valor": 16666,
            "juros": 300,
            "dataPrimeira": "2023-03-02"
        },
        "valor": 100000,
        "tipo": "saque-loja",
        "data": "2023-03-02"
    }
    ```



!!! attention "Observação"

    No JSON de envio, é necessário ter o valor solicitado do saque ($VALOR), e o objeto da solicitação ($OBJETO_SOLICITAÇÃO) que é criado a partir das informações do usuário como id, cpf, limite, etc... E outras informações como data e hora da solicitação.
    
    O objeto solicitação tem informações diferentes para cada um dos 2 fluxos de saque disponíveis. Um exemplo é o fluxo de saque por transferência bancária, este fluxo precisa ter o banco, agência, conta e o tipo da conta no objeto solicitação.

### Retorno:

=== "**Status 200 (Sucesso)**"

    ```json
    OK
    ```

=== "**Status 401 (Falha)**"

    ```json {linenums="1"}
    {
      "statusCode": 401,
      "error": "Unauthorized",
      "message": "Token nao informado!"
    }
    ```

<p align="right">
    <img src="../assets/images/koerich_logo.svg"/>
    <img src="../assets/images/klab_logo.png" width="152"/>
</p>
