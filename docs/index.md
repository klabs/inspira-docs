# Introdução

**Inspira Kab**, aplicação desenvolvida para tablet, cuja a funcionalidade é trazer comodidade e agilidade ao vendedor, no atendimento as necessidades do consumidor em relação a transações referente ao cartão de crédito Koerich.

![Inspira Kab](assets/images/inspira_kab.png)

## Features

* Login;
* Busca por cliente;
* Saque assistido;

## Atores

| Atores                | Descrição                                                                   |
| --------------------- | --------------------------------------------------------------------------- |
| Consumidor            | Beneficiário que será criada as operações.                                  |
| Vendedor              | Vendedor da loja que estará utilizando o Aplicativo Inspira Kab para realizar operações solicitadas para o consumidor |

## Arquitetura:

![Inspira Kab Flux](assets/images/diagrams/inspira_kab_flux.svg)

![Inspira Kab Flux](assets/images/diagrams/general_flux.jpg)

<p align="right">
    <img src="assets/images/koerich_logo.svg"/>
    <img src="assets/images/klab_logo.png" width="152"/>
</p>
