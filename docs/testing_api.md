# Api de Testes

URL base para as requisições é  **https://inspira-hml.klab.com.br**.

| Ação                    | Path                           | Método | Header  | Body  | Auth |
| ----------------------- | ------------------------------ | ------ | ------- | ----- | ---- |
| Login                   | /auth                          | POST   | [Authorization: Basic **base64(username:password)**](https://www.rfc-editor.org/rfc/rfc7617)| VAZIO | VAZIO |
| Autorização             | /auth/verify                   | POST   | VAZIO   | Content-type *application/x-www-form-urlencoded*<br />token  *token a ser validado* | VAZIO |
| Busca por cliente       | /res/cliente/$VALUE1/$VALUE2   | GET    | VAZIO   | VAZIO |  Bearer Token *token* |
| Listagem dos bancos     | /res/banco   | GET   | VAZIO   | VAZIO  | Bearer Token *token*   |
| Listagem tipo planos    | /res/prestacoes/cliente/${ID/CPF}/valor/${valor} | GET  | VAZIO   | VAZIO |  Bearer Token *token* |
| Saque assistido         | /res/cliente/saque/${ID/CPF}   | PUT  | Content-type *application/json* | JSON |  Bearer Token *token* |

!!! attention "Observação"

    A **busca por cliente** pode ser realizada de duas maneiras, por ID ou CPF:

    $VALUE1 - Determina o tipo da busca;
    $VALUE2 - Utiliza-se para inserir os dados referente ao tipo;

    Exemplo:

    $VALUE1 = id ou cpf;
    $VALUE2 = 1001 ou  98707445083;

    **Listagem tipo planos** é realizada informando o ID ou CPF numérico e o valor numérico 
    **Saque Assistido** é realizada informando o ID ou CPF numérico

<p align="right">
    <img src="../assets/images/koerich_logo.svg"/>
    <img src="../assets/images/klab_logo.png" width="152"/>
</p>
