# Change Logs

!!! attention "Alterações"

Após o projeto entrar em produção todas as alterações significativas serão informadas aqui, através de lista selecionada e ordenada cronologicamente.
