## Login:

=== "**Campos**"

    | Atributos      | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ |
    | username                |Nome de acesso do vendedor      |string|
    | password             |Senha de acesso do vendedor     |string|

=== "**Envio**"

    | Estrutura                       | Descrição                                                    | Tipo                 |
    | ------------------------------- | ------------------------------------------------------------ | -------------------- |
    | Basic Base64(username:password) | Conforme definido pelo [IETF](https://www.rfc-editor.org/rfc/rfc7617) | Authorization Header |

=== "**Retorno Sucesso**"

    | Campo                   | Descrição                      | Tipo | Valor |
    | ----------------------- | ------------------------------ | ------ | ------ |
    | access_token            |Token                           |string  |  |
    | token_type       |O Bearer Token é um recurso utilizado para garantir a segurança durante a utilização da API e informa as permissões que cada usuário da integração possui. É gerado na autenticação do usuário e será solicitado diversas vezes durante seu uso da API, a fim de garantir a autenticidade das requisições e evitar conflitos.     |string |Bearer |

=== "**Extração payload do access_token no formato JWT**"

    | Campo  | Descrição                                     | Tipo    |
    | ------ | --------------------------------------------- | ------- |
    | name   | Nome do vendedor                              | string  |
    | exp    | Carimbo da data de quando o token irá expirar | integer |
    | active | Se o token é valido (está ativo) ou não       | boolean |

=== "**Retorno Falha 401**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ | 
    | "message_error"        |Apenas mensagem que não está autorizado - unauthorized  |status HTTP  | 

## Autorização:

=== "**Envio**"

    | Estrutura               | Descrição                                                    | Tipo                 |
    | ----------------------- | ------------------------------------------------------------ | -------------------- |
    | Bearer {{access_token}} | Conforme definido pelo [IETF](https://www.rfc-editor.org/rfc/rfc6750) | Authorization Header |
    
    **Observação:** para que as solicitações sejam realizadas deve ser enviado o access_token.

=== "**Retorno 200**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ | 
    | active            |Se o token é valido (está ativo) ou não    |boolean|

=== "**Falha retorna  401**"

    | Campo      | Descrição                                                    | Tipo    |
    | ---------- | ------------------------------------------------------------ | ------- |
    | statusCode | Código numérico da falha                                     | integer |
    | error      | Tipo do erro                                                 | string  |
    | message    | Mensagem do porque que deu a falha ex: "Token não informado” | string  |


#### 

!!! attention "Erro de regra de negócio"

	No retorno o back-end deve usar essa estrutura:
	
	#### 	Falha retorna  400:
	
	| Campo   | Descrição                                                  | Tipo   |
	| ------- | ---------------------------------------------------------- | ------ |
	| message | Mensagem do porque que deu a falha ex: "Id não encontrado” | string |


## Busca por cliente:

=== "**Envio**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ |
    | cpf       |CPF do consumidor      |PathParam|
    | id |ID do consumidor |PathParam|



=== "**Retorno Sucesso**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ |
    |id           |Código do consumidor no ERP     |integer|
    |nome                |Nome do consumidor      |string |
    |email           |E-mail do consumidor    |string|
    |endereço          |Endereço do consumidor     |string|
    |nascimento           |Data de nascimento consumidor - “aaaa-mm-dd”    |string|
    |CPF                |CPF do consumidor      |string |
    |celular  |Celular do consumidor - “(99)99999-9999” não será validado| string|
    |limite |Limite total valor numérico em centavos disponível no Kab| |
    |limiteDisponivel|Limite disponível para saque valor numérico em centavos disponível no Kab|integer|
    |status|Status do cartão Kab|string|
    |statusMessage|Mensagem de status do cartão ex:"O cartão está ATIVO|string|
    |cartao / vencimento|Vencimento do cartão - mm/aaaa|string|
    |cartao / diaCorte|Dia de corte do cartão data de fechamento do cartão - dd|string|



## Lista bancos 

#### Retorno:

=== "**Sucesso retorna**"

    #### Lista com vários bancos, campos retornados:
    
    | Campo  | Descrição          | Tipo    |
    | ------ | ------------------ | ------- |
    | numero | Número do banco ID | integer |
    | nome   | Nome do banco      | string  |



## Saque assistido:



#### Tipo planos - fluxo saque em loja e transferência bancária:

#### 

#### Retorno:

=== "**Sucesso retorna**"

    #### Lista com os planos liberados para o consumidor, campos retornados:
    
    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ |
    | quantidade              |Quantidade de parcelas em forma numérica |integer|
    | valor                   |Valor de cada parcela em centavos de forma numérica|integer|
    | juros                   |Juros da parcela de forma numérica ex: 1,27% envia 127 - evitar perda de precisão|integer|
    | dataPrimeira          |Data de vencimento da primeira parcela|string|

#### 

### Solicitação do saque -  fluxo saque em loja e transferência bancária:

=== "**Envio**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ |
    | valor|Valor de saque  solicitado pelo consumidor|string|
    | objeto solicitação| Todos os dados coletados no fluxo ex: banco e loja |object|

=== "**Objeto solicitação Banco**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ |
    |idCliente          |Código do consumidor no ERP     |integer|
    |parcela / quantidade|Quantidade de parcela que o consumidor irá pagar|integer|
    |parcela / valor|Valor de cada parcela em centavos de forma numérica|integer|
    |parcela / juros|Juros da parcela de forma numérica ex: 1,27% envia 127 - evitar perda de precisão|integer|
    |parcela / dataPrimeira|Data de pagamento da primeira parcela aaaa-mm-dd|string|
    |banco / nome|Nome do banco onde o consumidor irá receber o saque|string|
    |banco / numero|Número do banco onde o consumidor irá receber o saque|integer|
    |conta / tipo|O tipo da conta que o consumidor irá receber o saque: ex: poupança ou conta corrente|string|
    |conta / agencia|Número da agência que o consumidor irá receber o saque|integer|
    |conta / numero|Número da conta que o consumidor irá receber o saque|integer|
    |valor|Valor em centavos que o consumidor solicitou de saque |integer|
    |tipo|O tipo como o consumidor irá sacar: transferência bancária |string|
    |data|Data de solicitação do saque aaaa-mm-dd|string|

=== "**Objeto solicitação loja**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ |
    |idCliente          |Código do consumidor no ERP     |integer|
    | parcela / quantidade   | Quantidade de parcela que o consumidor irá pagar             | integer |
    | parcela / valor        | Valor de cada parcela em centavos de forma numérica          | integer |
    | parcela / juros        |Juros da parcela de forma numérica ex: 1,27% envia 127 - evitar perda de precisão|integer|
    |parcela / dataPrimeira|Data de pagamento da primeira parcela aaaa-mm-dd|string|
    |valor|Valor em centavos que o consumidor solicitou de saque |integer|
    |tipo|O tipo como o consumidor irá sacar: saque em loja |string|
    |data|Data de solicitação do saque aaaa-mm-dd|string|


=== "**Sucesso retorna**"

    | Campo                   | Descrição                      | Tipo |
    | ----------------------- | ------------------------------ | ------ | 
    | ok               |A requisição foi enviada com sucesso     |string|
